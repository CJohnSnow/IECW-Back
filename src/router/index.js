import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [{
    path: '/',
    redirect: '/login',
    component: resolve => require(['@/components/page/Login'], resolve)
  },
  {
    path: '/login',
    component: resolve => require(['@/components/page/Login'], resolve),
    name: 'login'
  },
  {
    path: '/HomePage',
    component: resolve => require(['@/components/common/Home.vue'], resolve),
    meta: {
      title: '系统首页'
    },
    children: [
      {
        path: '/HomePage',
        component: resolve => require(['@/views/HomePage/home.vue'], resolve),
        meta: {
          title: '系统首页'
        }
      },
      { //任丽蓉  增加路由配置
        path: '/',
        redirect: '/HomePage',
        component: resolve => require(['@/components/common/Home.vue'], resolve),
        meta: {
          title: '资料库管理'
        }
      },
      {
        path: '/CertainBase',
        component: resolve => require(['@/views/DatabaseManage/CertainDatabase/CertainBase.vue'], resolve),
        meta: {
          title: '特定页面库管理'
        }
      },
      {
        path: '/ArticleDatabase',
        component: resolve => require(['@/views/DatabaseManage/ArticleDatabase/ArticleDatabase.vue'], resolve),
        meta: {
          title: '文章库管理'
        }
      },
      {
        path: '/AboutUs',
        component: resolve => require(['@/views/DatabaseManage/ArticleDatabase/AboutUs.vue'], resolve),
        meta: {
          title: '文章库关于我们'
        }
      },
      {
        path: '/FileManage',
        component: resolve => require(['@/views/DatabaseManage/FileManage/FileManage.vue'], resolve),
        meta: {
          title: '文件管理'
        }
      },
      {
        path: '/FileDetails',
        component: resolve => require(['@/views/DatabaseManage/FileManage/FileDetails.vue'], resolve),
        meta: {
          title: '文件库详情展示页面'
        }
      },
      {
        path: '/ArticleDetails',
        component: resolve => require(['@/views/DatabaseManage/ArticleDatabase/ArticalPageDetails.vue'], resolve),
        meta: {
          title: '文章库详情列表'
        }
      },
      {
        path: '/PhotoDatabase',
        component: resolve => require(['@/views/DatabaseManage/PhotoDatabase/PhotoDatabase.vue'], resolve),
        meta: {
          title: '图片库管理'
        }
      },
      {
        path: '/PhotoDetails',
        component: resolve => require(['@/views/DatabaseManage/PhotoDatabase/PhotoPageDetails.vue'], resolve),
        meta: {
          title: '图片库详情列表'
        }
      },
      {
        path: '/ViedoDatabase',
        component: resolve => require(['@/views/DatabaseManage/VideoDatabase/ViedoDatabase.vue'], resolve),
        meta: {
          title: '视频库管理'
        }
      },
      {
        path: '/ViedoDetails',
        component: resolve => require(['@/views/DatabaseManage/VideoDatabase/ViedoPageDetails.vue'], resolve),
        meta: {
          title: '视频库详情列表'
        }
      },
      {
        path: '/PublishArticle',
        component: resolve => require(['@/views/PublishArticle/PublishArticle.vue'], resolve),
        meta: {
          title: '发布文章'
        }
      },
      {
        path: '/FileManage',
        component: resolve => require(['@/views/DatabaseManage/FileManage/FileManage.vue'], resolve),
        meta: {
          title: '文件管理'
        }
      },
      {
        path: '/FileDetails',
        component: resolve => require(['@/views/DatabaseManage/FileManage/FileDetails.vue'], resolve),
        meta: {
          title: '文件库详情展示页面'
        }
      },
      // 管理员管理部分
      {
        path: '/AdministratorInfo',
        component: resolve => require(['@/views/AdministratorManage/AdministratorInfo/AdministratorInfo.vue'], resolve),
        meta: {
          title: '管理员信息'
        }
      },
      {
        path: '/SuperAdministratorInfo',
        component: resolve => require(['@/views/AdministratorManage/SuperAdministratorInfo/SuperAdministratorInfo.vue'], resolve),
        meta: {
          title: '超管信息'
        }
      },
      {
        path: '/ColumnManage',
        component: resolve => require(['@/views/ColumnManage/ColumnManage.vue'], resolve),
        meta: {
          title: '栏目管理'
        }
      },
      // 系统管理部分
      {
        path: '/WebsiteInfo',
        component: resolve => require(['@/views/SystemManage/WebsiteInfo/WebsiteInfo.vue'], resolve),
        meta: {
          title: '网站信息'
        }
      },
      {
        path: '/NavigationConfig',
        component: resolve => require(['@/views/SystemManage/NavigationConfig/NavigationConfig.vue'], resolve),
        meta: {
          title: '导航配置'
        }
      },
      {
        path: '/SwipperConfig',
        component: resolve => require(['@/views/SystemManage/SwipperConfig/SwipperConfig.vue'], resolve),
        meta: {
          title: '轮播配置'
        }
      },
      {
        path: '/FooterConfig',
        component: resolve => require(['@/views/SystemManage/FooterConfig/FooterConfig.vue'], resolve),
        meta: {
          title: '底部信息配置'
        }
      }
    ]
  },
  {
    path: '*',
    component: resolve => require(['../components/page/404.vue'], resolve),
    name: '404'
  }
  ]
})