import Vue from 'vue';
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    managerInfos: {
      id: '',
      workId: '',
      realname: '',
      lastLoginTime: '',
      createTime: '',
      status: '',
      roleId: '',
      token: ''
    },
  },
  getters: {
    token: state => state.token
  },
  mutations: {
    initManagerInfo (state, info) {
      state.managerInfos.id = info.managerInfo.id
      state.managerInfos.workId = info.managerInfo.workId
      state.managerInfos.realname = info.managerInfo.realname
      state.managerInfos.lastLoginTime = info.managerInfo.lastLoginTime
      state.managerInfos.createTime = info.managerInfo.createTime
      state.managerInfos.status = info.managerInfo.status
      state.managerInfos.roleId = info.managerInfo.roleId
      state.managerInfos.token = info.token
      console.log(state.managerInfos)
    },
    setManagerInfo (state, managerInfo) {
      state.managerInfo = managerInfo
    }
  },
  actions: {},
  modules: {}

})
export default store
