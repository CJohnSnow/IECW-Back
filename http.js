import axios from "axios"
import {
    Loading,
    Message
} from "element-ui"
import router from './src/router/index';


let loading

function startLoading() {
    loading = Loading.service({
        lock: true,
        text: '加载中',
        background: 'rgba(0,0,0,0,7)'
    })
}

function endLoading() { //使用Element loading-close 方法
    loading.close()
}
//请求拦截
axios.interceptors.request.use((config) => {
        startLoading()
        if (localStorage.eleToken) {
            //统一的请求头
            config.headers.token = localStorage.eleToken
        }
        return config
    },
    error => {
        return Promise.reject(error)
    })

//响应
axios.interceptors.response.use((response) => {
        endLoading()

        return response
    },
    error => {
        endLoading()
        Message.error(error.response.data)
        if (status == 401) {
            Message.error('token 失效,重新登录')
            //清除token
            localStorage.removeItem('eleToken')

            router.push('/login')
        }

        return Promise.reject(error)
    })



export default axios
